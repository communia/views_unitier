VIEWS UNITIER
=============

To merge two views, despite it is with the aim of join via sql, its so difficult, for a different 
entity types to be joined, so, apart of the utilities of the service unitier, it will be done in php side.

Add a views area in header in the main view of type simple combine view, here it will let you choose
an auxiliar view to be combined, and report you which sorts are matching between these two views. 
The main view sort order will be used to pass to the array_multisort. No pager, no filters, and partially
sorts are supported.
