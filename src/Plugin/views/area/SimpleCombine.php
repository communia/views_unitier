<?php

namespace Drupal\views_unitier\Plugin\views\area;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\area\View;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area handlers. Insert a view inside of an area.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("views_unitier_simple_combine")
 */
class SimpleCombine extends View {
  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['inherit_arguments'] = [
	      '#type' => 'checkbox',
	        '#title' => $this->t('Inherit contextual filters'),
	          '#default_value' => $this->options['inherit_arguments'],
	        '#description' => $this->t('If checked, this view will receive the same contextual filters as its parent.'),
    ];

    $sorts = [];
    $matches = [];
    if (!empty($this->options['view_to_insert'])) {
      [$aux_view_name, $display_id] = explode(':', $this->options['view_to_insert']);

      $aux_view = $this->viewStorage->load($aux_view_name)->getExecutable();

      if (empty($aux_view) || !$aux_view->access($display_id)) {
        return [];
      }
      $aux_view->setDisplay($display_id);

      // Avoid recursion
      $aux_view->parent_views += $this->view->parent_views;
      $aux_view->parent_views[] = "$aux_view_name:$display_id";

      // Check if the view is part of the parent views of this view
      $search = "$aux_view_name:$display_id";
      if (in_array($search, $this->view->parent_views)) {
        \Drupal::messenger()->addError(t("Recursion detected in view @view display @display.", ['@view' => $aux_view_name, '@display' => $display_id]));
      }
      else {
        foreach ($this->view->getHandlers('sort') as $sort_key => $sort_value){
          if (isset($aux_view->getHandlers('sort')[$sort_key])){
            if ($aux_view->getHandlers('sort')[$sort_key]['entity_type'] == $sort_value['entity_type'])
             $matches[] = $sort_key;
          }
          $sorts[$sort_key] = $sort_key;
        }
      }
    }

    $form['matching_sorts'] = array(
      '#type' => 'checkboxes',
      '#options' => $sorts,
      //'#default_value' => array_values($sorts),
      '#default_value' => $matches,
      '#title' => $this->t('Computed merged sorts that will be applied'),
      '#description' => $this->t('Only the items matching in both views will be applied. Unchecked boxes means non existing in attached view. Due it is done in php it cannot respect neither pager, nor filters'),
      '#disabled' => true
    );

  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    return [];
  }
  /**
   * Combines views based on configuration.
   *
   */
  public function combineViews() {
    if (!empty($this->options['view_to_insert'])) {
      [$aux_view_name, $aux_display_id] = explode(':', $this->options['view_to_insert']);

      $aux_view = $this->viewStorage->load($aux_view_name)->getExecutable();

      if (empty($aux_view) || !$aux_view->access($aux_display_id)) {
        return [];
      }
      $aux_view->setDisplay($aux_display_id);

      // Avoid recursion
      $aux_view->parent_views += $this->view->parent_views;
      $aux_view->parent_views[] = "$aux_view_name:$aux_display_id";

      // Check if the view is part of the parent views of this view
      $search = "$aux_view_name:$aux_display_id";
      if (in_array($search, $this->view->parent_views)) {
        \Drupal::messenger()->addError(t("Recursion detected in auxiliary view @aux_view display @aux_display.", ['@aux_view' => $aux_view_name, '@display' => $aux_display_id]));
      }
      else {
        $aux_view->setItemsPerPage(10);
        $aux_view->setOffset(0);
        $aux_view->usePager();
        $aux_view->execute();

        if (!empty($this->options['inherit_arguments']) && !empty($this->view->args)) {
          //$output = $aux_view->preview($display_id, $this->view->args);
          $aux_view->setArguments($this->view->args);
        }
        /*else {
          $output = $aux_view->preview($display_id);

        }*/
        $this->isEmpty = $aux_view->display_handler->outputIsEmpty();
        $result = $this->view->result;

        $view_entity_types = $this->getEntityTypes($this->view);
        $aux_view_entity_types =  $this->getEntityTypes($aux_view);
        $map_entity_types = $this->mapEntityTypes($view_entity_types, $aux_view_entity_types);
        
        $matched_sorts = $this->getMatchedSorts($aux_view);
        if (count($result) > 0 && (count($aux_view->result) > 0)) {
          foreach ($aux_view->result as $i => $aux_view_row) {
            // clone last element of the results to allocate new row
            // Cannot just slice as it will be used the value as reference 
            // so both values will be affected in next lines.
            $result[] = new \Drupal\views\ResultRow((array) array_slice($result, -1)[0]);

            foreach($map_entity_types as $relationship_id => $aux_relationship_id) {
              $aux_entity = ($aux_relationship_id == 'base')? $aux_view_row->_entity:$aux_view_row->_relationship_entities[$aux_relationship_id];
              if ($relationship_id == 'base') { 
                $result[array_key_last($result)]->_entity = $aux_entity;
              } else {
                $result[array_key_last($result)]->_relationship_entities[$relationship_id] = $aux_entity;
              }
              // Fill the matched sorts field values to allow the final multisort
              // based on matched values
              foreach($matched_sorts as $matched_field_alias => $aux_matched_field_alias) {
                $result[array_key_last($result)]->{$matched_field_alias} = $aux_view_row->{$aux_matched_field_alias['aux_alias']}; //$aux_entity->{$aux_matched_field_alias['aux_alias']}->getString();
              }
            }
          }
        } elseif (count($result) == 0 && (count($aux_view->result) > 0)) {
          $user_roles = \Drupal::currentUser()->getRoles();                              
          if (in_array('administrator', $user_roles)) {                                  
            \Drupal::messenger()->addError(t("Main view (@view display: @display) has no results, it is a need for auxiliary view ( @aux_view display @aux_display ) results to be
 combined.", [                                                                                                                                                                    
              '@view' => $this->view->id(),
              '@display'  => $this->view->current_display,                               
              '@aux_view' => $aux_view_name,                                          
              '@aux_display' => $aux_display_id                                          
            ]));                                                                                                                                                                  
          }     
        }
      }
    }
    // Prepare the multisort to plasmate in each row as index property
    $sort_keys = [];
    foreach ($matched_sorts as $matched_field_alias => $aux_matched_field_alias) {
      $sort_keys[] = array_map(function($n) use($matched_field_alias) { return $n->$matched_field_alias;}, $result);
      $sort_keys[] = $aux_matched_field_alias['order'];
    }
    $sort_keys[] = &$result;
    array_multisort(...$sort_keys);
    // apply the order index in index property
    foreach($result as $sorted_index => $item) {
      $item->index = $sorted_index;
    }
    // Apply pager limit to not overflow the requested quatity of rows.
    $items_per_page = $this->view->getPager()->options['items_per_page'];
    if ($items_per_page?:count($result) < count($result)) {
      array_splice($result, $items_per_page);
    }    
    return $result;
  }

  /**
   * Get map of the entity types, that a view has through base or relationship.
   *
   * @param $view Drupal\views_unitier\Plugin\views\area\ViewExecutable
   *   The view to get entity types from
   *
   * @return array
   *   Array keyed with base or the relationship id
   */
  private function getEntityTypes($view) {
    // Get the entity to fill based on the entities existing on ViewExecutable::getBaseEntityType and this->view->getHandlers('relationship')
    $view_entity_types = [];
    $view_entity_types['base'] = $view->getBaseEntityType()->id();
    foreach($view->getHandlers('relationship') as $relationship_id => $relationship) {
      if (!isset($relationship['entity_type'])) {
        $view_entity_types[$relationship_id] = ($relationship["field"] == 'nid' and $relationship["table"] == 'node__feeds_item')? 'node':null;
      } else {
        $view_entity_types[$relationship_id] = $relationship['entity_type'];        if ($view_entity_types[$relationship_id] == 'group_content') {
          $view_entity_types[$relationship_id] = 'node';
        }
      }
    }
    return $view_entity_types;
  }

  /**
   * Get map of the entity types, related to other view.
   *
   * @param $view_entity_types array
   *   The main view entities ids 
   * @param $aux_view_entity_types array 
   *   The aux view entities ids 
   *
   * @return array
   *   Array keyed with base or the relationship id pointing to aux matching id
   */
  private function mapEntityTypes($view_entity_types, $aux_view_entity_types) {
    $map = [];
    foreach ($view_entity_types as $relationship => $view_entity_type) {
      $match_entity_type = array_search($view_entity_type, $aux_view_entity_types);
      if ($match_entity_type) $map[$relationship] = $match_entity_type;
    }
    return $map;
  }

  /**
   * Get the sorts that are matching in both views.
   *
   * @param $aux_view Drupal\views_unitier\Plugin\views\area\ViewExecutable
   *   The auxiliary view.
   *
   * @return array
   *   Array keyed with the field id that are matching in both views.
   */

  private function getMatchedSorts(ViewExecutable $aux_view) {
    $matches = [];
    $matches_aliased = [];
    $sorts = [];
    foreach ($this->view->getHandlers('sort') as $sort_key => $sort_value){
      if (isset($aux_view->getHandlers('sort')[$sort_key])){
        if (
          //$aux_view->getHandlers('sort')[$sort_key]['entity_type'] == $sort_value['entity_type'] && 
          $aux_view->getHandlers('sort')[$sort_key]['field'] == $sort_value['field'] &&
          $aux_view->getHandlers('sort')[$sort_key]['table'] == $sort_value['table']
        ) {
          // $matches[] = $sort_key; // if doesnt need the alias
          // else fill with the exact alias for each side.
          foreach($this->view->build_info['query']->getFields() as $field_alias => $field ){
            $aux_sort_alias_id = $this->getSortFieldAlias($aux_view, $sort_value['field'], $sort_value['table']);
            if ($field['field'] == $sort_value['id'] ) {
              $matches_aliased[$field_alias] = [
                'aux_alias' => $aux_sort_alias_id,
                'order' => ($sort_value["order"]=='ASC')? SORT_ASC : SORT_DESC 
              ];
              break;
            }
          }
        }
      }
      $sorts[$sort_key] = $sort_key;
    }
    return $matches_aliased;
  }

  /**
   * Auxiliary function to get alias in view from field and table ids. Valid for
   * sort as may be more ocurrences for fields.
   *
   *  @param $view Drupal\views_unitier\Plugin\views\area\ViewExecutable
   *    The view to inspect.
   *  @param $field string
   *    The field looked for
   *  @param $table string
   *    The table where field lives.
   *
   *  @return string
   *    The alias the specified field points to.
   */
  private function getSortFieldAlias(ViewExecutable $view, $field, $table) {
    foreach($view->build_info['query']->getFields() as $field_alias => $select_field) {
      if ($field == $select_field['field'] && $table == $view->build_info['query']->getTables()[$select_field['table']]['table']){
        return $field_alias;
      }
    }
  }
}
