<?php

namespace Drupal\views_unitier;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Unitier service.
 */
class Unitier {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs an Unitier object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * Loads a view from configuration and returns its executable object.
   *
   * @param string $id
   *   The view ID to load.
   *
   * @return \Drupal\views\ViewExecutable|null
   *   A view executable instance or NULL if the view does not exist.
   */
  public static function getView($id) {
    $view = $this->entityTypeManager()
      ->getStorage('view')
      ->load($id);
    if ($view) {
      return static::executableFactory()
        ->get($view);
    }
    return NULL;
  }

  /**
   * Will return an array of aliases keyed by real_name.
   *
   * As for its nature it will assign last alias found for each
   * real table. So don't use it when complex queries with many alias pointing
   * to same table.
   *
   * @param \Drupal\views\Plugin\views\query\QueryPluginBase $view_query
   *   The current query plugin.
   *
   * @return array
   *   An array of table aliases keyed by its real_name.
   */
  public function getRealTables($view_query) {
    $view_real_tables = [];

    foreach ($view_query->getTableQueue() as $table_alias => $view_table) {
      $view_real_tables[$view_table['table']] = $table_alias;
    }
    return $view_real_tables;
  }

  /**
   * Will return an array of equivalence between two views fields.
   *
   * @param \Drupal\views\Plugin\views\query\QueryPluginBase
   * $view_left_query
   *   The left query plugin so the provider of keys of return fields array.
   * @param \Drupal\views\Plugin\views\query\QueryPluginBase
   * $view_right_query
   *   The right query plugin so the provider of value of return fields array.
   *
   * @return array
   *   An array of the field equivalence keyed by left query fields and valued
   *   by its equivalence in right query fields.
   */
  public function getFieldsEquivalence($view_left_query, $view_right_query) {
    $view_left_real_tables = $this->getRealTables($view_left_query);
    $view_right_real_tables = $this->getRealTables($view_right_query);
    $rosetta = [];
    foreach ($view_left_query->fields as $left_field_alias => $left_field) {
      foreach($view_right_query->fields as $right_field_alias => $right_field) {
        // To fill rosetta for this field must match tables
        if ($view_left_query->getTableQueue()[$left_field["table"]]['table'] == ($view_right_query->getTableQueue()[$right_field["table"]]['table'])) {
          // and must match field
          if ($left_field["field"] == $right_field["field"]) {
            $rosetta[$left_field["alias"]] = $right_field["alias"];
            // end assigning loop for this alias as we got this alias just set
            continue 2;
          }
        }
      }
    }
    return $rosetta;
  }


}
